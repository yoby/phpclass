<?php
function getpage($page=1,$tid=1)//page获取1页  tid 1是精选2是闪电购
{
    $url = "https://api.m.jd.com/client.action";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_REFERER, "https://prodev.m.jd.com/");
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36 Edg/93.0.961.47");
    curl_setopt($ch, CURLOPT_URL, $url);
    $header[] = "Content-Type: application/x-www-form-urlencoded";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $s = urlencode('{"geo":{"lng":"","lat":""},"tabId":"' . $tid . '","page":' . $page . ',"version":2,"source":"default","previewTime":""}');
    $d = 'appid=newtry&functionId=try_feedsList&uuid=&clientVersion=&client=wh5&osVersion=&area=1_72_2819_0&networkType=&body=' . $s;
    curl_setopt($ch, CURLOPT_POSTFIELDS, $d);
    $data = curl_exec($ch);
    curl_close($ch);
    $arr = json_decode($data, 1);
    $arr = $arr["data"]["feedList"];
    $arrs = [];
    foreach ($arr as $v) {
        $n=$v["supplyNum"]??0 ;
        $price=round($v["trialPrice"]);
        if ($n>= 2 && $price<=1) {
            $arrs[] = ["id" => $v["trialActivityId"],
                "trialname" => $v["skuTitle"],
                "largeimage" => $v["skuImg"],
                "applycount" => $v["applyNum"],
                "supplycount" => $v["supplyNum"],
                "orderprice" => $price,

            ];
        }

    }
  return $arrs;
}
$b=[];
for($i=1;$i<=10;$i++){
    $a = getpage($i);

    if (count($a)>0){
     foreach ($a as $v){
         $b[]=$v;
     }
    }
}

for($i=1;$i<=10;$i++){
    $c= getpage($i,2);
    if (count($c)>0){
        foreach ($c as $v){
            $b[]=$v;
        }
    }
}
header('Content-type: application/json');
echo json_encode($b,JSON_UNESCAPED_UNICODE);