<?php
/*
 * 获取股票主题
 */
/**
 * @param $arr
 * @param false $reserveKey 是否保留key
 * @return array|mixed
 */
function ArrayUnique($arr, $reserveKey = false)
{
    if (is_array($arr) && !empty($arr))
    {
        foreach ($arr as $key => $value)
        {
            $tmpArr[$key] = serialize($value) . '';
        }
        $tmpArr = array_unique($tmpArr);
        $arr = array();
        foreach ($tmpArr as $key => $value)
        {
            if ($reserveKey)
            {
                $arr[$key] = unserialize($value);
            }
            else
            {
                $arr[] = unserialize($value);
            }
        }
    }
    return $arr;
}

$arrs = [
    "a07"=>"银行",
	"a08"=>"保险",
	"010003"=>"房地产",
	"010005"=>"煤炭",
	"010009"=>"水泥",
	"010008"=>"有色",
	"010006"=>"家电",
	"002001"=>"白酒",
	"e03006"=>"环保设备",
	"002006"=>"旅游",
	"071"=>"猪肉",
	"a79"=>"hit电池",
	"003"=>"军工",
	"002002"=>"医药",
	"a26"=>"化工",
	"010004"=>"钢铁",
	"xx"=>"稀土永磁",
	"a171"=>"储能",
	"a168"=>"光伏建筑",
	"051"=>"天然气",
	"a11"=>"风电",
	"yy"=>"太阳能",
	"a204"=>"发电概念",
	"003003"=>"大飞机",
	"040"=>"氟化工",
	"g01"=>"盐湖提锂",
	"041"=>"草甘膦",
	"a20"=>"石油",
	"a144"=>"有机硅",
	"a200"=>"磷化工",
	"a153"=>"化肥",
	"a170"=>"钠离子",
	"038"=>"钛白粉",
    "a03"=>"券商",
    "a193"=>"工业母机",
    "081"=>"半导体",
    "001"=>"新能源车",
    "002003"=>"食品饮料",
];
$arrx = [//对应板块
    "a07"=>"90.BK0475",
    "a08"=>"90.BK0474",
    "010003"=>"90.BK0451",
    "010005"=>"90.BK0437",
    "010009"=>"90.BK0424",
    "010008"=>"90.BK0478",
    "010006"=>"90.BK0456",
    "002001"=>"90.BK0896",
    "e03006"=>"90.BK0494",
    "002006"=>"90.BK0485",
    "071"=>"90.BK0882",
    "a79"=>"90.BK0908",
    "003"=>"90.BK0490",
    "002002"=>"90.BK0727",
    "a26"=>"90.BK0512",
    "010004"=>"90.BK0479",
    "xx"=>"90.BK0578",
    "a171"=>"90.BK0989",
    "a168"=>"90.BK0978",
    "051"=>"90.BK0843",
    "a11"=>"90.BK0595",
    "yy"=>"90.BK0588",
    "a204"=>"90.BK1014",
    "003003"=>"90.BK0814",
    "040"=>"90.BK0690",
    "g01"=>"90.BK0987",
    "041"=>"90.BK0950",
    "a20"=>"90.BK0464",
    "a144"=>"90.BK0961",
    "a200"=>"90.BK1010",
    "a153"=>"90.BK0731",
    "a170"=>"90.BK0988",
    "038"=>"90.BK0805",
    "a03"=>"90.BK0473",
    "a193"=>"90.BK1004",
    "081"=>"90.BK0917",
    "001"=>"90.BK0900",
    "002003"=>"90.BK0438",
];
$keyarr = array_keys($arrs);
$type = $_GET["type"]??"index";
if ($type=="index") {//板块涨跌
    $a = [];
        foreach ($arrs as $kk=>$vv) {
        if(in_array($kk,$keyarr)){
           $ar =  getbk($kk);
            $a[] = [
                "code" => $kk,
                "name" => $vv,
                "chg" => $ar["chg"],
                "jia" => $ar["jia"],
                "money" => $ar["money"],
                "price" => $ar["price"],
                "prices" => $ar["prices"],
            ];
        }

        }

    $a = ArrayUnique($a);
}elseif($type=="vi"){//大盘指数
   $url="http://push2.eastmoney.com/api/qt/ulist.np/get?fields=f1,f2,f3,f4,f14&secids=1.000001,0.399001,0.399006,1.000300,1.000905,1.000688,100.HSI,100.DJIA,100.NDX,100.FTSE,114.jmm,114.im";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($ch);
    curl_close($ch);
    $arr = json_decode($data, true);
    $arr = $arr['data']["diff"];
    $a = [];
    foreach ($arr as $vv){
        $n = $vv["f1"]==2?100:10;
        $a[] = [
            "name"=>$vv["f14"],
            "pie"=>strval($vv["f2"]/$n),
            "chg"=>strval($vv["f3"]/100)."%",
            "addpie"=>strval($vv["f4"]/$n),
        ];
    }

}elseif($type=="open"){//判断是否开市
    //$url = "http://push2.eastmoney.com/api/qt/stock/get?fields=f43,f57,f58,f169,f170,f46,f44,f51,f168,f47,f164,f163,f116,f60,f45,f52,f50,f48,f167,f117,f71,f161,f49,f530,f135,f136,f137,f138,f139,f141,f142,f144,f145,f147,f148,f140,f143,f146,f149,f55,f62,f162,f92,f173,f104,f105,f84,f85,f183,f184,f185,f186,f187,f188,f189,f190,f191,f192,f107,f111,f86,f177,f78,f110,f262,f263,f264,f267,f268,f250,f251,f252,f253,f254,f255,f256,f257,f258,f266,f269,f270,f271,f273,f274,f275,f127,f199,f128,f193,f196,f194,f195,f197,f80,f280,f281,f282,f284,f285,f286,f287,f292&secid=0.300750";
    $url = "http://push2.eastmoney.com/api/qt/stock/get?fields=f292&secid=0.300750";
    $as = [
        1=>"开盘竞价","交易中","盘中休息",
        4=>"收盘竞价","已收盘","停牌",
        7=>"退市","暂停上市","未上市",
        10=>"未开盘","盘前","盘后",
        13=>"节假日休市","盘中停牌","非交易代码",
        16=>"波动性中断","盘后交易启动","盘后集中撮合交易","盘后固定价格交易"

    ];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($ch);
    curl_close($ch);
    $arr = json_decode($data, true);
    $arr = $arr['data']["f292"];
    $t = date("w");
    if ($arr==13){
       $a=[
           "status"=>"0",//休市
           "msg"=>"休市"
       ] ;
    }else{
        if($t==0 || $t==6){
            $a=[
                "status"=>"0",//休市
                "msg"=>"休市"
            ] ;
        }else{
            $a=[
                "status"=>"1",//开市
                "msg"=>"开市"
            ] ;
        }

    }

}else{//板块下股票
    $code = $_GET["code"]??"a70";//默认代码
    $size = $_GET["size"]??10;//每页显示
    $url = "http://quote.eastmoney.com/zhuti/api/themerelatestocks?CategoryCode=$code&startIndex=1&pageSize=$size";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($ch);
    curl_close($ch);
    $arr = json_decode($data, true);
    $arr = $arr['result'][0]["Data"];
    $a=[];
    foreach ($arr as $vv){
      $ar =   explode("|",$vv);
      $a[]=[
          "code"=>$ar[0],
          "qcode"=>$ar[1],
          "name"=>$ar[2],
          "desc"=>$ar[5],
          "price"=>$ar[7],
          "chg"=>$ar[8]."%",

      ];
    }
}
header("Access-Control-Allow-Origin: *");
header('Content-type: application/json');
exit(json_encode($a, JSON_UNESCAPED_UNICODE));

/*获取板块下涨跌*/
function getbk($code){
    global $arrs,$arrx;
    if(empty($arrs[$code])){
        $a=[];
    }else {
        $code1=$arrx[$code];
        $url = "http://push2.eastmoney.com/api/qt/stock/get?secid=$code1&fields=f43,f170,f169,f113,f114,f115,f48";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        curl_close($ch);
        $arr = json_decode($data, true);
        $arr = $arr["data"];
        $a = [
            "code" => $code,
            "name" => $arrs[$code],
            "jia" => $arr["f113"] . "|" . $arr['f115'] . "|" . $arr['f114'],
            "money" => round(floatval($arr['f48'] / 100000000), 2) . "亿",//金额
            "prices" => (string)floatval($arr['f43'] / 100),//当前点数
            "price" => (string)floatval($arr['f169'] / 100),//点数
            "chg" => floatval($arr['f170'] / 100) . "%",//涨幅

        ];
    }
    return $a;

}
