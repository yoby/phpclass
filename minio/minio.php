<?php

require 'vendor/autoload.php';
use Aws\Exception\AwsException;
$s3 = new Aws\S3\S3Client([
        'version' => 'latest',
        'region'  => 'us-east-1',
        'endpoint' => 'http://localhost:9000',
        'use_path_style_endpoint' => true,
        'credentials' => [
                'key'    => 'P55OPS3GBBSGO0GBB8VH',
                'secret' => 'VjTfrznY0PB+BghMU5Tgtl6GU52zA7oOHjVGYyyI',
            ],
]);

// 发送PutObject请求并获得result对象
$insert = $s3->putObject([
     'Bucket' => 'xxx1',
     'Key'    => 'testkey',
     'Body'   => 'Hello from MinIO!!'
]);

// 下载文件的内容
$retrive = $s3->getObject([
     'Bucket' => 'xxx1',
     'Key'    => 'testkey',
     'SaveAs' => 'testkey_local'
]);

// 通过索引到结果对象来打印结果的body。
echo $retrive['Body'];

$result = $s3->listBuckets();
$array = $result->toArray();
//dump($array);
$names = $result->search('Buckets[].Name');//只返回桶
dump($names);

try {
    $s3->createBucket(['Bucket' => 'abcd']);//桶需要至少四个字母
}catch (AwsException $exception){}

$results = $s3->getPaginator('ListObjects', ['Bucket' => 'localimg']);
foreach ($results->search('Contents[].Key') as $key) {
    echo $key . "\n";
}
$list = $s3->listObjects(["Bucket"=>"localimg","key"=>"images"]);
foreach ($list['Contents'] as $object) {
    echo $object['Key'] . "\n";
}

$s3->putObject([
    'Bucket' =>"abcd",
    'Key' => "1.jpg",
    'SourceFile' => "1.webp",
]);//上传文件
$resp = $s3->getObjectUrl('localimg', 'images/profile.9ea78635.jpg');
dump($resp);//获取url