composer require intervention/image //php.=5.4 支持laravel
include_once "vendor/autoload.php";
use Intervention\Image\ImageManager  as Image;
$image = new Image();
//$image->widen(300);//高度等比例缩放
//$image->heighten(300);//宽度等比例
//$image->resize(300,null);//高度不变 resize(800, 800);
//$image->width(); //宽度
//$image->height();//高度
//blur(10) 模糊度 0-100 越大越模糊速度也很慢
//opacity(20) 透明度 很慢
//crop(100, 100, 25, 25);//图片的裁剪，默认左上角开始裁剪，宽，高，可选x,y坐标
//destroy() 删除图片
//flip('h') 左右翻转 v 上下翻转
//fit(600,600,function ($c){$c->upsize();}); //按比例修剪图片
//greyscale() 图片灰色
$img = $image->make('1.webp');
//top-left (default) top top-right left center right bottom-left bottom bottom-right 九个位置,x轴,y轴
//$img->insert('1.png' ,'center',10,10);//加入水印,合并图片
//$img->response('png');//转换格式支持 jpg,png,gif,tif,bmp
//$img->rotate(-45);//旋转角度
//$img->sharpen(100);//锐化,0-100
//$img->pixelate(20);//添加马赛克
//echo $img->mime();//图片格式头
/*$img->text("中国人你好吗",150,50,function ($f){
    $f->file(dirname(__FILE__).'/zk.ttf')
->size(24)
        ->color('#fdf6e3')
->align("center")//left center right
        ->angle(0)//旋转角度
    ->valign('top');//top middle bottom
});*/
//echo $img->pickColor(50,50,'hex');座标颜色
//dump($img->exif());//图片信息数组
//echo $img->encode();//图片编码
//$img->save('bar.jpg');
//下载网络图片此方法最快
/*$url="http://wx.qlogo.cn/mmopen/XxT9TiaJ1ibf06TNRCMjQADS4opDHvQLguLZHpqkRlvuJYZicvJW4iaOalPsKIs0kpZ3F6864ZzibyObYiaucUQSrdp4pFTNDyIpxw/0";
$data=curl($url);
$img = $image->make($data);
$img->save("2.jpg");*/

/*$img = $image->canvas(500, 500);//画图,默认背景透明
$img->circle(100, 50, 50, function ($draw) {//在图片里面画圆，直径，x坐标，y坐标
    $draw->background('#fff');//背景色
    $draw->border(1, '#ff0');//圆边框，与背景色
});
$img->ellipse(100, 50, 90, 70, function ($draw) {//定义椭圆跟圆一致
    $draw->background('#0000ff');
    $draw->border(1, '#ff0');
});
$img->rectangle(5, 5, 50, 50, function ($draw) {//画矩形，可选背景色（颜色类型可选rgba，和普通）
    $draw->background('rgba(255, 255, 255, 0.5)');
    $draw->border(2, '#000');
});*/
//echo $img->filesize();//文件大小字节
$img->save("2.webp");
